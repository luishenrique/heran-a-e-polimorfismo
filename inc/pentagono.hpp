#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica {
    
    private:
        //Atributos
        float lado;
        float apotema;
        
    public:
        //Métodos construtores
        Pentagono();
        Pentagono(float lado, float apotema);
        Pentagono(string tipo, float lado, float apotema);
        ~Pentagono();

        //Métodos acessores
        void set_lado(float lado);
        float get_lado();
        void set_apotema(float apotema);
        float get_apotema();

        //Outros métodos
        float calcula_area();
        float calcula_perimetro();
        void imprime_forma();
};

#endif