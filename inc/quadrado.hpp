#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica {
        
    public:
        //Métodos construtores
        Quadrado();
        Quadrado(float base, float altura);
        Quadrado(string tipo, float base, float altura);
        ~Quadrado();

        //Outros métodos
        void imprime_forma();

};

#endif