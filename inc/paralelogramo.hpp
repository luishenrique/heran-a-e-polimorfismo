#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica {

    public:
        //Métodos construtores
        Paralelogramo();
        Paralelogramo(float base, float altura);
        Paralelogramo(string tipo, float base, float altura);
        ~Paralelogramo();

        //Outros métodos
        void imprime_forma();
        

};

#endif