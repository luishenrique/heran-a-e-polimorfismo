#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"

class Hexagono : public FormaGeometrica {

    private:
        //Atributos
        float lado;

    public:
        //Métodos construtores
        Hexagono();
        Hexagono(float lado);
        Hexagono(string tipo, float lado);
        ~Hexagono();

        //Métodos acessores
        void set_lado(float lado);
        float get_lado();

        //Outros métodos
        float calcula_area();
        float calcula_perimetro();
        void imprime_forma();
};

#endif