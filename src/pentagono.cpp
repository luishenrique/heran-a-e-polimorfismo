#include <iostream>
#include "pentagono.hpp"

Pentagono::Pentagono() {
    set_tipo("Pentágono regular");
    set_lado(7.265f);
    set_apotema(5.0f);
}

Pentagono::Pentagono(float lado, float apotema) {
    set_tipo("Pentágono regular");
    set_lado(lado);
    set_apotema(apotema);    
}

Pentagono::Pentagono(string tipo, float lado, float apotema) {
    set_tipo(tipo);
    set_lado(lado);
    set_apotema(apotema);
}

Pentagono::~Pentagono() {}

void Pentagono::set_lado(float lado) {
    this->lado = lado;
}
float Pentagono::get_lado() {
    return lado;
}

void Pentagono::set_apotema(float apotema) {
    this->apotema = apotema;
}
float Pentagono::get_apotema() {
    return apotema;
}

float Pentagono::calcula_perimetro() {
    return (get_lado() * 5);
}

float Pentagono::calcula_area() {
    return ((get_lado() * 5) * get_apotema())/2;
}

void Pentagono::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Lado: " << get_lado() << endl;
    cout << "Apotema: " << get_apotema() << endl;
    cout << "Área: " << calcula_area() << "cm²" << endl;
    cout << "Perímetro: " << calcula_perimetro() << "cm" << endl;
}