#include <iostream>
#include <math.h>
#include "hexagono.hpp"

Hexagono::Hexagono() {
    set_tipo("Hexágono regular");
    set_lado(8.0f);
}

Hexagono::Hexagono(float lado) {
    set_tipo("Hexágono regular");
    set_lado(lado);
}

Hexagono::Hexagono(string tipo, float lado) {
    set_tipo(tipo);
    set_lado(lado);
}

Hexagono::~Hexagono() {}

void Hexagono::set_lado(float lado) {
    this->lado = lado;
}
float Hexagono::get_lado() {
    return lado;
}

float Hexagono::calcula_area() {
    return ((6 * (get_lado()*get_lado()) * sqrt(3))/4 );
}
float Hexagono::calcula_perimetro() {
    return get_lado() * 6;
}

void Hexagono::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Lados: " << get_lado() << endl;
    cout << "Área: " << calcula_area() << "cm²" << endl;
    cout << "Perímetro: " << calcula_perimetro() << "cm" << endl;
}