#include <iostream>
#include "circulo.hpp"

using namespace std;

Circulo::Circulo() {
    set_tipo("Circulo");
    set_raio(5.0f);
}

Circulo::Circulo(float raio) {
    set_tipo("Circulo");
    set_raio(raio);
}

Circulo::Circulo(string tipo, float raio) {
    set_tipo(tipo);
    set_raio(raio);
}

Circulo::~Circulo() {}

void Circulo::set_raio(float raio) {
    this->raio = raio;
}
float Circulo::get_raio() {
    return raio;
}

float Circulo::calcula_area() {
    float pi = 3.14f;
    return pi * (get_raio() * get_raio());
}

float Circulo::calcula_perimetro() {
    float pi = 3.14f;
    return 2 * pi * get_raio();
}

void Circulo::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Raio: " << get_raio() << endl;
    cout << "Área: " << calcula_area() << "cm²" << endl;
    cout << "Perímetro: " << calcula_perimetro() << "cm" << endl;
 }