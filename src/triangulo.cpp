#include <iostream>
#include <math.h>
#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo() {
    set_tipo("Triângulo retângulo");
    set_base(6.0f);
    set_altura(10.0f);
}

Triangulo::Triangulo(float base, float altura) {
    set_tipo("Triângulo retângulo");
    set_base(base);
    set_altura(altura);
}

Triangulo::Triangulo(string tipo,float base, float altura) {
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Triangulo::~Triangulo () {}

float Triangulo::calcula_area() {
    return (get_base() * get_altura()) / 2.0f;
}

float Triangulo::calcula_perimetro() {
    float hipotenusa = 0;

    hipotenusa = sqrt(pow(get_base(),2) + pow(get_altura(),2));
    
    return hipotenusa + get_base() + get_altura();
}

void Triangulo::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Base: " << get_base() << endl;
    cout << "Altura " << get_altura() << endl;
    cout << "Área: " << calcula_area() << "cm²" << endl;
    cout << "Perímetro: " << calcula_perimetro() << "cm" << endl;
}