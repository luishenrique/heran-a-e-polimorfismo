#include <iostream>
#include "paralelogramo.hpp"

using namespace std;

Paralelogramo::Paralelogramo() {
    set_tipo("Paralelogramo");
    set_base (10.0f);
    set_altura (5.0f);
}

Paralelogramo::Paralelogramo(float base, float altura) {
    set_tipo("Paralelogramo");
    set_base(base);
    set_altura(altura);
}

Paralelogramo::Paralelogramo(string tipo, float base, float altura) {
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Paralelogramo::~Paralelogramo() {}

void Paralelogramo::imprime_forma() {
    cout << "Tipo: " << get_tipo() << endl;
    cout << "Base: " << get_base() << endl;
    cout << "Altura: " << get_altura() << endl;
    cout << "Área: " << calcula_area() << "cm²" << endl;
    cout << "Perímetro: " << calcula_perimetro() << "cm" << endl;
}