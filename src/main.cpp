#include <iostream>
#include <string>
#include <vector>
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

using namespace std;

string getString() {
    string valor;
    getline(cin, valor);
    return  valor;
}

template <typename T1>
T1 getInput() {
    while(true) {
        T1 valor;
        cin >> valor;
        if(cin.fail()) {
            cin.clear();
            cin.ignore(32767, '\n');
            cout << "Entrada inválida, insira novamente!" << endl;
        } else {
            cin.ignore(32767, '\n');
            return valor;
        }
    }
}

int main () {
    
    vector <Triangulo *> triangulos;
    vector <Quadrado *> quadrados;
    vector <Circulo *> circulos;
    vector <Paralelogramo *> paralelogramos;
    vector <Pentagono *> pentagonos;
    vector <Hexagono *> hexagonos;
    vector <FormaGeometrica *> formas;

    int comando = -1;

    while (comando != 0) {
        //cout << "=================================" << endl;
        cout << "Geometria Plana" << endl;
        cout << "Criar nova forma geométrica" << endl;
        //cout << "---------------------------------" << endl;
        cout << "(1) Inserir objeto geométrico" << endl;
        cout << "(2) Imprime lista por objeto" << endl;
        cout << "(3) Imprime lista geral" << endl;
        cout << "(0) Sair" << endl;
        cout << "::" <<endl;
        comando = getInput<int>();

        string tipo;
        float base;
        float altura;
        float raio;
        float lado;
        float apotema;
    
        switch(comando) {
            case 1:
                cout << "---------------------------" << endl;
                cout << "Inserir objeto" << endl;
                cout << "---------------------------" << endl;
                cout << "Tipo: ";
                tipo = getString();

                if(tipo == "Triangulo Retângulo" || tipo == "triangulo retângulo" || tipo == "Triângulo Retângulo" || tipo == "triângulo retângulo" || tipo == "Triângulo retângulo" || tipo == "Triangulo retangulo" || tipo == "Triângulo" || tipo == "Triangulo" || tipo == "triângulo" || tipo == "triangulo"){
                    cout << "Base: ";
                    base = getInput<float>();
                    cout << "Altura: ";
                    altura = getInput<float>();

                    triangulos.push_back(new Triangulo(tipo, base, altura));
                    formas.push_back(new Triangulo(tipo, base, altura));
                
                } else if (tipo == "Quadrado" || tipo == "quadrado") {
                    cout << "Base: ";
                    base = getInput<float>();
                    cout << "Altura: ";
                    altura = getInput<float>();

                    if(base == altura) {
                        quadrados.push_back(new Quadrado(tipo, base, altura));
                        formas.push_back(new Quadrado(tipo, base, altura));
                    } else {
                        cout << "Quadrado inválido!" << endl;
                    }

                } else if (tipo == "Circulo" || tipo == "circulo" || tipo == "Círculo" || tipo == "círculo") {
                    cout << "Raio: ";
                    raio = getInput<float>();

                    circulos.push_back(new Circulo(tipo, raio));
                    formas.push_back(new Circulo(tipo, raio));

                } else if(tipo == "Paralelogramo" || tipo == "paralelogramo"){
                
                    cout << "Base: ";
                    base = getInput<float>();
                    cout << "Altura: ";
                    altura = getInput<float>();
                    
                    paralelogramos.push_back(new Paralelogramo(tipo, base, altura));
                    formas.push_back(new Paralelogramo(tipo, base, altura));
                
                } else if (tipo == "Pentágono Regular" || tipo == "pentágono regular" || tipo == "Pentagono Regular" || tipo == "pentagono regular" || tipo == "Pentágono regular" || tipo == "Pentagono regular" || tipo == "Pentágono" || tipo == "pentágono" || tipo == "Pentagono" || tipo == "pentagoono") {
                    cout << "Lados[cm]: ";
                    lado = getInput<float>();
                    cout << "Apotema: ";
                    apotema = getInput<float>();

                    pentagonos.push_back(new Pentagono(tipo, lado, apotema));
                    formas.push_back(new Pentagono(tipo, lado, apotema));

                } else if (tipo == "Hexagono"||tipo == "hexagono"|| tipo == "Hexágono"|| tipo == "hexágono" || tipo == "Hexágono Regular" || tipo == "hexágono regular" || tipo == "Hexágono regular" || tipo == "Hexagono regular") {
                    
                    cout << "Lados[cm]: ";
                    lado = getInput<float>();

                    hexagonos.push_back(new Hexagono(tipo, lado));
                    formas.push_back(new Hexagono(tipo, lado));

                } else {
                    cout << "Objeto invalido!" << endl;
                }
                
                break;
            
            case 2:
                cout << "-------------------------" << endl;
                cout << "Digite o tipo de objeto: ";
                tipo = getString();
                cout << "-------------------------" << endl;

                if(tipo == "Triangulo" || tipo == "triangulo" || tipo == "Triângulo" || tipo == "triângulo") {
                    
                    cout << "==========================" << endl;
                    cout << "Lista de triangulos" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Triangulo *t : triangulos) {
                        t -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }

                } else if (tipo == "Quadrado" || tipo == "quadrado") {

                    cout << "==========================" << endl;
                    cout << "Lista de quadrados" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Quadrado * q: quadrados) {
                        q -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }
                    
                } else if (tipo == "Circulo" || tipo == "circulo" || tipo == "Círculo" || tipo == "círculo") {

                    cout << "==========================" << endl;
                    cout << "Lista de circulos" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Circulo *c : circulos) {
                        c -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }

                } else if (tipo == "Paralelogramo" || tipo == "paralelogramo") {

                    cout << "==========================" << endl;
                    cout << "Lista de paralelogramos" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Paralelogramo *p : paralelogramos) {
                        p -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }
                
                } else if (tipo == "Pentágono" || tipo == "pentágono" || tipo == "Pentagono" || tipo == "pentagono") {

                    cout << "==========================" << endl;
                    cout << "Lista de pentagonos" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Pentagono *pen : pentagonos) {
                        pen -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }
                
                } else if (tipo == "Hexagono"||tipo == "hexagono"|| tipo == "Hexágono"|| tipo == "hexágono" || tipo == "Hexágono Regular" || tipo == "hexágono regular" || tipo == "Hexágono regular" || tipo == "Hexagono regular") {
                    
                    cout << "==========================" << endl;
                    cout << "Lista de hexagonos" << endl;
                    cout << "--------------------------" << endl;
                    
                    for(Hexagono *hex : hexagonos) {
                        hex -> imprime_forma();
                        cout << "-------------------------" << endl;
                    }

                } else {
                    cout << "Objeto não listado!" << endl;
                }

                break;

            case 3:
                cout << "=========================" << endl;
                cout << "Lista Geral" << endl;
                cout << "-------------------------" << endl;

                for(FormaGeometrica * f: formas) {
                    f -> imprime_forma();
                    cout << "-----------------------" << endl;
                }
                break;

            case 0:
                break;

            default:
                cout << "Opção inválida! Insira novamente!" << endl;
        }
           
    }

    return 0;
}